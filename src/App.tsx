import React, { FC } from "react";
import Movies from "./components/movies/Movies";

const App: FC = () => {
  return <Movies />;
};

export default App;
