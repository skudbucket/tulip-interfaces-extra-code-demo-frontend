import React, { FC, useEffect, useState } from "react";
import LoadingIndicator from "../LoadingIndicator";
import MoviesResults from "./MoviesResults";
import MoviesSearch from "./MoviesSearch";
import searchMovies from "./services/searchMovies.service";
import MovieData from "./types/MovieData.interface";
import MoviesSearchFormData from "./types/MoviesSearchFormData.interface";
import SearchMoviesParams from "./types/SearchMoviesParams.interface";
import SearchMoviesResponse from "./types/SearchMoviesResponse.interface";

const Movies: FC = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [searchString, setSearchString] = useState<string>();
  const [searchResponse, setSearchResponse] = useState<SearchMoviesResponse>();
  const [aggregatedSearchResults, setAggregatedSearchResults] =
    useState<MovieData[]>();

  const doSearch = async (searchFormData: MoviesSearchFormData) => {
    setSearchString(searchFormData.search);
  };

  useEffect(() => {
    if (searchString !== undefined) {
      const getFirstPage = async () => {
        setIsLoading(true);
        setSearchResponse(undefined);
        setAggregatedSearchResults(undefined);

        const searchParams: SearchMoviesParams = {
          substr: searchString,
        };
        const currentSearchResponse = await searchMovies(searchParams);

        setSearchResponse(currentSearchResponse);
        setAggregatedSearchResults(currentSearchResponse.results);
        setIsLoading(false);
      };

      getFirstPage();
    }
  }, [searchString]);

  useEffect(() => {
    if (searchString !== undefined && searchResponse !== undefined) {
      const getNextPage = async () => {
        setIsLoading(true);

        const currentSearchResponse = await searchMovies({
          substr: searchString,
          page: searchResponse.page + 1,
        });

        setAggregatedSearchResults((existingResults) =>
          (existingResults || []).concat(currentSearchResponse.results)
        );
        setSearchResponse(currentSearchResponse);

        setIsLoading(false);
      };

      const { page, totalNumPages } = searchResponse;

      if (page < totalNumPages) {
        getNextPage();
      }
    }
  }, [searchResponse]);

  return (
    <div className="movies">
      <MoviesSearch onSearch={doSearch} />
      {isLoading ? (
        <LoadingIndicator />
      ) : (
        <>
          {aggregatedSearchResults !== undefined ? (
            <MoviesResults results={aggregatedSearchResults} />
          ) : (
            <></>
          )}
        </>
      )}
    </div>
  );
};

export default Movies;
