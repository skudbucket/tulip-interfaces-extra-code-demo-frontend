import React, { FC } from "react";
import MovieData from "./types/MovieData.interface";

interface MovieResultProps extends Omit<MovieData, "uid"> {}

const MovieResult: FC<MovieResultProps> = ({ title, year }) => {
  return (
    <div>
      {title} ({year})
    </div>
  );
};

export default MovieResult;
