import React, { FC } from "react";
import MovieResult from "./MovieResult";
import MovieData from "./types/MovieData.interface";

interface MoviesResultsProps {
  results: MovieData[];
}

const MoviesResults: FC<MoviesResultsProps> = ({ results }) => {
  return (
    <div className="movie-results">
      {results.length === 0
        ? "No results found."
        : results.map((movieData) => {
            const { uid, ...movieResultProps } = movieData;

            return <MovieResult key={uid} {...movieResultProps} />;
          })}
    </div>
  );
};

export default MoviesResults;
