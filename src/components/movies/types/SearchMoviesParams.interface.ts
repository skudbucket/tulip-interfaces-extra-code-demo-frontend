interface SearchMoviesParams {
  substr: string;
  page?: number;  
}

export default SearchMoviesParams;
