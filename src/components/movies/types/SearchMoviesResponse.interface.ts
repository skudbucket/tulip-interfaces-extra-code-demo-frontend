import MovieData from "./MovieData.interface";

interface SearchMoviesResponse {
  totalNumResults: number;
  page: number;
  totalNumPages: number;
  numResultsPerPage: number;
  numResults: number;
  results: MovieData[];
}

export default SearchMoviesResponse;
