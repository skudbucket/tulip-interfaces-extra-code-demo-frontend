interface MoviesSearchFormData {
  search: string;
}

export default MoviesSearchFormData;
