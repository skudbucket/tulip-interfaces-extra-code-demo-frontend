interface MovieData {
  uid: string;
  title: string;
  year: number;
}

export default MovieData;
