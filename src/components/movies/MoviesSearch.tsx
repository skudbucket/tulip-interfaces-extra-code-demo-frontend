import React, { FC } from "react";
import { useForm } from "react-hook-form";
import MoviesSearchFormData from "./types/MoviesSearchFormData.interface";

const MIN_SEARCH_STRING_LENGTH = 3;

interface MoviesSearchProps {
  onSearch: (searchFormData: MoviesSearchFormData) => void;
}

const MoviesSearch: FC<MoviesSearchProps> = ({ onSearch }) => {
  const {
    formState: { errors, isDirty, isValid },
    handleSubmit,
    register,
  } = useForm<MoviesSearchFormData>({
    mode: "all",
  });

  const doSubmit = (formData: MoviesSearchFormData) => onSearch(formData);

  return (
    <form onSubmit={handleSubmit(doSubmit)} className="form">
      <div className="form-item">
        <label htmlFor="search">Search movies by title</label>
        <input
          {...register("search", {
            minLength: {
              value: MIN_SEARCH_STRING_LENGTH,
              message: `Please enter at least ${MIN_SEARCH_STRING_LENGTH} characters to search`,
            },
          })}
          id="search"
        />
        {errors?.search ? (
          <div className="error-message">{errors.search?.message}</div>
        ) : (
          <></>
        )}
      </div>
      <input
        type="submit"
        value="Search"
        disabled={!(isDirty && isValid)}
        className="search-button"
      />
    </form>
  );
};

export default MoviesSearch;
