import SearchMoviesParams from "../types/SearchMoviesParams.interface";
import SearchMoviesResponse from "../types/SearchMoviesResponse.interface";

const searchMovies = async (
  params: SearchMoviesParams
): Promise<SearchMoviesResponse> => {
  const url = new URL("http://kenskudder.com/tulip/faux-movies-api.php");

  const urlSearchParams = new URLSearchParams(Object.entries(params));

  url.search = urlSearchParams.toString();

  const response = await fetch(url.toString());

  if (!response.ok) {
    throw new Error("Request failed!");
  }

  return await response.json();
};

export default searchMovies;
