# About This Demo

This is a demo I made to illustrate some of my coding skills that I felt I wasn't able to really show in the HackerRank test (I had some issues with the HackerRank online coding environment). In particular, I tried to code something which would show my competency with retrieving data from APIs and validating form fields in React, which is what I believe the last 2 questions of the HackerRank test were about. I based the functionality of this demo on what I remembered of the requirements for the HackerRank test's last 2 questions which I had read briefly during the test. Specifically, the requirements I implemented this demo to fulfill are:

- Create a form with a text input that a user can use to search for movies.
- Validate the form to require at least 3 characters to be entered in the search text input.
- Validate the text input on each keypress.
- The backend API will return paginated results if there are more than 10 results, and does not offer a way to request all results, so the frontend must make multiple requests to retrieve all results if there are more than 10 results and aggregate the data from the multiple requests.

I did a small amount of basic styling, just to make the demo more usable, but I did not focus on styling, in accordance with the requirements of the HackerRank test.

I didn't think to note the URL of the mock API used in the HackerRank test and couldn't remember it, so I coded a custom backend which functions the same as how I remember the mock API in the HackerRank test to function.

You can see this demo live hosted at: http://kenskudder.com/tulip/demo/

I wrote all of this code myself, without any help (beyond the usual - checking MDN to refresh my memory on syntax from time to time, occasionally Googling things). It took me about 6 hours, split about evenly between the frontend and the backend.
